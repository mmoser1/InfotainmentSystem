<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:34a00e67-7b39-4966-bffa-0020349b4927(MySecurityAnalysis)">
  <persistence version="9" />
  <languages>
    <devkit ref="9b903ecd-ba57-441e-8d7c-d3f1fbfcc047(com.moraad)" />
  </languages>
  <imports>
    <import index="qiur" ref="r:8720e696-0fac-4176-a4dc-80083fb5401b(MethodConfiguration)" />
    <import index="qxlf" ref="r:291748f2-3cc9-439c-b64a-e488f1504975(Catalog)" />
    <import index="wwjg" ref="r:6b7672de-8b71-4266-a291-90689a64256c(Infotainment_Firmware_Update_Feature)" />
  </imports>
  <registry>
    <language id="d8c07454-d390-4e04-8826-d25e86f59134" name="de.itemis.mps.xdiagram">
      <concept id="69042634962010496" name="de.itemis.mps.xdiagram.structure.XDiagramLayoutEntry" flags="ng" index="zGsxD">
        <property id="69042634962010502" name="layoutString" index="zGsxJ" />
        <property id="1174236820146112338" name="elementId" index="2MHvPS" />
      </concept>
      <concept id="69042634962010499" name="de.itemis.mps.xdiagram.structure.XDiagramLayoutStorage" flags="ng" index="zGsxE">
        <property id="2498010886192733022" name="rootId" index="1ueiNO" />
        <child id="69042634962010500" name="layoutEntries" index="zGsxH" />
      </concept>
      <concept id="280164805027066272" name="de.itemis.mps.xdiagram.structure.XDiagram" flags="ng" index="3xPMB7">
        <child id="69042634962010512" name="layoutStorages" index="zGsxT" />
      </concept>
    </language>
    <language id="8aedd025-5f31-4a1e-81a1-4c5345407211" name="com.moraad.suggestions">
      <concept id="1744555010771063086" name="com.moraad.suggestions.structure.AssThreatScenarioAssistantSuggestionFactory" flags="ng" index="k5Jq$" />
      <concept id="1744555010771063084" name="com.moraad.suggestions.structure.AssAdditionThreatScenarioAssistantSuggestion" flags="ng" index="k5JqA" />
      <concept id="1744555010776338337" name="com.moraad.suggestions.structure.RelationAssistantSelector" flags="ng" index="khAwF" />
      <concept id="1744555010776336880" name="com.moraad.suggestions.structure.DamageScenarioAssistantSelector" flags="ng" index="khATU" />
      <concept id="1744555010776327868" name="com.moraad.suggestions.structure.ThreatScenarioAssistantSelector" flags="ng" index="khC4Q" />
      <concept id="114192864337941166" name="com.moraad.suggestions.structure.AssAdditionDamageScenarioAssistantSuggestion" flags="ng" index="raIdw" />
      <concept id="1920997147008949188" name="com.moraad.suggestions.structure.RiskAssistantSelector" flags="ng" index="CEhHY" />
      <concept id="1920997147009089272" name="com.moraad.suggestions.structure.AssRiskSuggestionFactory" flags="ng" index="CENT2" />
      <concept id="8675225129845988701" name="com.moraad.suggestions.structure.AssDsThreatenedByTsSuggestionFactory" flags="ng" index="2FpSCn" />
      <concept id="8675225129768242352" name="com.moraad.suggestions.structure.AssistantChunk" flags="ng" index="2Q15JU">
        <child id="1744555010776060220" name="assistantSelector" index="kmFqQ" />
        <child id="8675225129778034533" name="factories" index="2Q$E0J" />
      </concept>
      <concept id="8675225129768254214" name="com.moraad.suggestions.structure.AssSuggestionQueryResult" flags="ng" index="2Q16Lc">
        <reference id="8675225129798856842" name="consultedNode" index="2ClQv0" />
        <child id="8675225129775903758" name="suggestions" index="2QGid4" />
      </concept>
      <concept id="8675225129768254215" name="com.moraad.suggestions.structure.AssSuggestion" flags="ng" index="2Q16Ld">
        <reference id="8675225129798855691" name="suggestedEntity" index="2ClRH1" />
        <reference id="8675225129815039074" name="consultedNode" index="2Dj$GC" />
      </concept>
      <concept id="8675225129768254213" name="com.moraad.suggestions.structure.AssSuggestionFactory" flags="ng" index="2Q16Lf">
        <child id="2567848404456432646" name="groups" index="3N3N22" />
      </concept>
      <concept id="446196523655023050" name="com.moraad.suggestions.structure.AssDamageScenarioSuggestionFactory" flags="ng" index="3aivMl" />
      <concept id="446196523655766478" name="com.moraad.suggestions.structure.AssResultGroup" flags="ng" index="3aHhih">
        <property id="6842080042029996409" name="identifier" index="133MFP" />
        <property id="446196523655779532" name="heading" index="3aHm6j" />
        <child id="446196523655778962" name="results" index="3aHmvd" />
      </concept>
    </language>
    <language id="2283d35c-b541-4c39-bf04-8310ba3f92ff" name="com.moraad.reports">
      <concept id="5638758366197687768" name="com.moraad.reports.structure.RiskTreatmentTableReportItem" flags="ng" index="21ek43" />
      <concept id="5662992976716575613" name="com.moraad.reports.structure.ThreatScenariosAndAttackPathsReportItem" flags="ng" index="28bWPA">
        <property id="5844418852561495228" name="limit" index="1CBqX7" />
      </concept>
      <concept id="2050517468709281410" name="com.moraad.reports.structure.AssetsAndDamageScenariosTableReportItem" flags="ng" index="ckFx4" />
      <concept id="6986877318773217091" name="com.moraad.reports.structure.TextReportItem" flags="ng" index="yg4y$">
        <property id="6986877318773333397" name="text" index="ygo9M" />
      </concept>
      <concept id="6986877318773201239" name="com.moraad.reports.structure.ComponentDiagramReportItem" flags="ng" index="ygSqK">
        <reference id="1019912726748740255" name="diagram" index="2HTkYB" />
      </concept>
      <concept id="6986877318773203685" name="com.moraad.reports.structure.RiskTableReportItem" flags="ng" index="ygVO2" />
      <concept id="6986877318773203683" name="com.moraad.reports.structure.ControlsTableReportItem" flags="ng" index="ygVO4" />
      <concept id="6986877318773203681" name="com.moraad.reports.structure.ThreatTableReportItem" flags="ng" index="ygVO6" />
      <concept id="6986877318773203653" name="com.moraad.reports.structure.AssumptionTableReportItem" flags="ng" index="ygVOy" />
      <concept id="6986877318772884603" name="com.moraad.reports.structure.RiskDistributionChartReportItem" flags="ng" index="yhPIs">
        <property id="6552748594463309586" name="showPreview" index="2DBfkM" />
      </concept>
      <concept id="6986877318772702512" name="com.moraad.reports.structure.ProjectInfoReportItem" flags="ng" index="ym6bn">
        <reference id="635552504747772140" name="projectInfo" index="39i2te" />
      </concept>
      <concept id="6986877318772759009" name="com.moraad.reports.structure.EmptyReportItem" flags="ng" index="ymko6" />
      <concept id="6986877318770896277" name="com.moraad.reports.structure.ResultReportChunk" flags="ng" index="ypf9M">
        <child id="6986877318770902011" name="items" index="yp9Ks" />
      </concept>
      <concept id="1488669593885577694" name="com.moraad.reports.structure.CommentReportItem" flags="ng" index="2JOk35">
        <property id="1488669593885577696" name="text" index="2JOk3V" />
      </concept>
      <concept id="5952104409253523121" name="com.moraad.reports.structure.TableOfContentsReportItem" flags="ng" index="3x3r7t" />
      <concept id="2129184553233376960" name="com.moraad.reports.structure.FuncAssignmentSmartTableReportItem" flags="ng" index="3xdgjh" />
      <concept id="2129184553237592657" name="com.moraad.reports.structure.DataTableReportItem" flags="ng" index="3xttx0" />
      <concept id="2129184553237592667" name="com.moraad.reports.structure.DataFlowsTableReportItem" flags="ng" index="3xttxa" />
      <concept id="2129184553237592647" name="com.moraad.reports.structure.ComponentsTableReportItem" flags="ng" index="3xttxm" />
      <concept id="2129184553237592677" name="com.moraad.reports.structure.ChannelsTableReportItem" flags="ng" index="3xttxO" />
      <concept id="2129184553237375048" name="com.moraad.reports.structure.FunctionTableReportItem" flags="ng" index="3xuwDp" />
      <concept id="2129184553228409378" name="com.moraad.reports.structure.FuncAssignmentSimpleTableReportItem" flags="ng" index="3xSvwN" />
      <concept id="2195216638865431028" name="com.moraad.reports.structure.DamageAndThreatScenariosReportItem" flags="ng" index="3yVM0i" />
      <concept id="8588388912954219383" name="com.moraad.reports.structure.DamageScenarioTableReportItem" flags="ng" index="3UIwP1" />
    </language>
    <language id="edd58c45-9999-4ad9-8f8a-e0d26da1cbc9" name="de.itemis.ysec.commons">
      <concept id="7050052209586915341" name="de.itemis.ysec.commons.structure.IChunkWithDefaultContent" flags="ng" index="2xH1$G">
        <child id="7050052209586915342" name="defaultContent" index="2xH1$J" />
      </concept>
      <concept id="3384350556523616640" name="de.itemis.ysec.commons.structure.ISecABasicElementRef" flags="ng" index="122Z_A">
        <reference id="3384350556523616658" name="target" index="122Z_O" />
      </concept>
      <concept id="2596867816763073964" name="de.itemis.ysec.commons.structure.IDescribed" flags="ng" index="1ALOwD">
        <child id="7057631560081871838" name="description" index="2JHqPs" />
      </concept>
      <concept id="2596867816763073961" name="de.itemis.ysec.commons.structure.ITitled" flags="ng" index="1ALOwG">
        <property id="1729603031951941283" name="title" index="DVXpC" />
      </concept>
    </language>
    <language id="d66daea8-e7a8-4305-aeaa-7ca535d07bd3" name="com.moraad.projectinfo">
      <concept id="9003278715588766811" name="com.moraad.projectinfo.structure.EmptyProjectInfoContent" flags="ng" index="$sJSh" />
      <concept id="9003278715588766803" name="com.moraad.projectinfo.structure.ProjectInfoListEntry" flags="ng" index="$sJSp">
        <child id="459042386150007873" name="freetextValue" index="X3RNv" />
      </concept>
      <concept id="9003278715588766804" name="com.moraad.projectinfo.structure.ProjectInfoList" flags="ng" index="$sJSu">
        <child id="9003278715588979763" name="listEntries" index="$tzTT" />
      </concept>
      <concept id="4299407153799527256" name="com.moraad.projectinfo.structure.ProjectInfoTable" flags="ng" index="39leHu" />
      <concept id="7449413747451491361" name="com.moraad.projectinfo.structure.ProjectInfoChunk" flags="ng" index="3eC5pO">
        <child id="9003278715588858344" name="projectInfoContent" index="$s4ey" />
      </concept>
    </language>
    <language id="048a18dc-8dce-4fe2-8e99-0a16464f630c" name="de.itemis.mps.editor.freetext">
      <concept id="8926592809623411165" name="de.itemis.mps.editor.freetext.structure.BasicParagraph" flags="ng" index="3VMn$0">
        <child id="8926592809623411166" name="runs" index="3VMn$3" />
      </concept>
      <concept id="8926592809623411162" name="de.itemis.mps.editor.freetext.structure.WordRun" flags="ng" index="3VMn$7" />
      <concept id="8926592809623411159" name="de.itemis.mps.editor.freetext.structure.Freetext" flags="ng" index="3VMn$a">
        <child id="8926592809623411163" name="paragraphs" index="3VMn$6" />
      </concept>
      <concept id="8926592809623411170" name="de.itemis.mps.editor.freetext.structure.IRun" flags="ng" index="3VMn$Z">
        <property id="8926592809623411171" name="text" index="3VMn$Y" />
      </concept>
    </language>
    <language id="a97beefa-b088-4bdb-8ed8-6b4e554b6264" name="com.moraad.sequences">
      <concept id="8142618915233841375" name="com.moraad.sequences.structure.SequencesChunk" flags="ng" index="1YSUgs" />
    </language>
    <language id="174fc1bc-8a89-4d07-8636-8bc5dc4757e4" name="de.itemis.vcs_text.tables">
      <concept id="312446707538163884" name="de.itemis.vcs_text.tables.structure.SimpleTable" flags="ng" index="2mR0c">
        <child id="312446707538413839" name="header" index="2hO6J" />
        <child id="312446707538164015" name="rows" index="2mR6f" />
      </concept>
      <concept id="312446707538163885" name="de.itemis.vcs_text.tables.structure.SimpleRow" flags="ng" index="2mR0d">
        <child id="312446707538164018" name="cells" index="2mR6i" />
      </concept>
      <concept id="312446707538163886" name="de.itemis.vcs_text.tables.structure.SimpleCell" flags="ng" index="2mR0e">
        <child id="312446707538454950" name="entry" index="2hY46" />
      </concept>
      <concept id="312446707540923383" name="de.itemis.vcs_text.tables.structure.EmptyCell" flags="ng" index="2opHn" />
      <concept id="312446707540702486" name="de.itemis.vcs_text.tables.structure.HeaderNameCellEntry" flags="ng" index="2ozQQ" />
      <concept id="4299407153800462969" name="de.itemis.vcs_text.tables.structure.FreetextCellEntry" flags="ng" index="38D_9Z">
        <child id="4299407153800463780" name="value" index="38D_my" />
      </concept>
    </language>
    <language id="24e88a55-f0b5-4dc5-9077-49730e920865" name="de.itemis.ysec.checklist">
      <concept id="6217398072109638567" name="de.itemis.ysec.checklist.structure.ChecklistItem" flags="ng" index="2H0S4X">
        <child id="4258253476795566208" name="rationale" index="3GS99T" />
      </concept>
      <concept id="6217398072109638633" name="de.itemis.ysec.checklist.structure.ChecklistItemGroup" flags="ng" index="2H0S5N">
        <child id="6217398072109638643" name="items" index="2H0S5D" />
      </concept>
      <concept id="6217398072109318275" name="de.itemis.ysec.checklist.structure.Checklist" flags="ng" index="2H3I8p">
        <property id="117579728711752679" name="__ItemsTextReadOnly" index="3F1M74" />
        <child id="6217398072109638590" name="groups" index="2H0S4$" />
      </concept>
      <concept id="4258253476795643009" name="de.itemis.ysec.checklist.structure.ChecklistItemHead" flags="ng" index="3GSqTS" />
    </language>
    <language id="2bca1aa3-c113-4542-8ac2-2a6a30636981" name="com.moraad.core">
      <concept id="4718052244458560179" name="com.moraad.core.structure.SecurityAnalysisChunk" flags="ng" index="2vPz$R">
        <child id="4718052244458560183" name="elements" index="2vPz$N" />
      </concept>
      <concept id="7050052209585848527" name="com.moraad.core.structure.ThreatContentSelector" flags="ng" index="2xx57I" />
      <concept id="7050052209585848529" name="com.moraad.core.structure.ControlContentSelector" flags="ng" index="2xx57K" />
      <concept id="7050052209585848531" name="com.moraad.core.structure.AssumptionContentSelector" flags="ng" index="2xx57M" />
      <concept id="7050052209585848533" name="com.moraad.core.structure.RiskContentSelector" flags="ng" index="2xx57O" />
      <concept id="7050052209585848535" name="com.moraad.core.structure.ScenarioContentSelector" flags="ng" index="2xx57Q" />
      <concept id="8071121944254209035" name="com.moraad.core.structure.DamageScenarioContentSelector" flags="ng" index="U8VUI" />
      <concept id="4601417698506916745" name="com.moraad.core.structure.EmptyAnalysisElement" flags="ng" index="19qcqd" />
      <concept id="330802076191738031" name="com.moraad.core.structure.RiskTreatmentEvaluation" flags="ng" index="1mMvoj">
        <property id="330802076205393171" name="lastUpdatedTimestamp" index="1nI1IJ" />
      </concept>
      <concept id="330802076190598253" name="com.moraad.core.structure.RiskTreatmentChunk" flags="ng" index="1mQ_Fh">
        <child id="330802076191738029" name="riskTreatments" index="1mMvoh" />
      </concept>
      <concept id="7050052209577206632" name="com.moraad.core.structure.ThreatScenarioContentSelector" flags="ng" index="3u6799" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="c1497963-7ffd-4da0-9a4d-74675c5ab7e2" name="com.moraad.components">
      <concept id="4903305818773966639" name="com.moraad.components.structure.TOEChunk" flags="ng" index="2lbcm6" />
      <concept id="4903305818773971546" name="com.moraad.components.structure.TOEComponent" flags="ng" index="2lbezN" />
      <concept id="4903305818773998197" name="com.moraad.components.structure.ITOEElementContainer" flags="ng" index="2lbk3s">
        <child id="4903305818773998200" name="elements" index="2lbk3h" />
      </concept>
      <concept id="3911760519739995188" name="com.moraad.components.structure.SystemDiagram" flags="ng" index="2ndE_3">
        <property id="7226555355291517223" name="typeOfVisibleConnections" index="2x$e3D" />
        <property id="1514418932059619330" name="hierarchyLevels" index="2zzwJW" />
        <child id="3260991312725364852" name="newDataChunk" index="1BS0SA" />
        <child id="3260991312725608311" name="newDataFlowsChunk" index="1BT5$_" />
        <child id="7472593337833908268" name="rootComponent" index="3Vepgw" />
      </concept>
      <concept id="7050052209593327461" name="com.moraad.components.structure.TOEFunctionContentSelector" flags="ng" index="2x4$T4" />
      <concept id="7050052209593327464" name="com.moraad.components.structure.TOEDataContentSelector" flags="ng" index="2x4$T9" />
      <concept id="7050052209593327466" name="com.moraad.components.structure.TOEComponentContentSelector" flags="ng" index="2x4$Tb" />
      <concept id="7050052209593327468" name="com.moraad.components.structure.TOEChannelContentSelector" flags="ng" index="2x4$Td" />
      <concept id="5188113475688114801" name="com.moraad.components.structure.FunctionAssignmentChunk" flags="ng" index="2zckJ6" />
      <concept id="4601417698506916745" name="com.moraad.components.structure.EmptyTOEElement" flags="ng" index="19qcqe" />
      <concept id="3043868224835494634" name="com.moraad.components.structure.TOEChannel" flags="ng" index="3mlHNJ">
        <child id="6453420821188241049" name="endPoints" index="38xWUi" />
      </concept>
      <concept id="4250072277178649485" name="com.moraad.components.structure.TOEChunkRef" flags="ng" index="3$0O6U">
        <reference id="4250072277178649488" name="target" index="3$0O6B" />
      </concept>
      <concept id="4250072277178649596" name="com.moraad.components.structure.TOEComponentRef" flags="ng" index="3$0O7b" />
    </language>
  </registry>
  <node concept="1mQ_Fh" id="6GvChUD1oSg">
    <property role="TrG5h" value="Risk Treatment" />
    <node concept="1mMvoj" id="6GvChUD1oSh" role="1mMvoh">
      <property role="1nI1IJ" value="1532013827941" />
    </node>
  </node>
  <node concept="2lbcm6" id="6GvChUD1oSi">
    <property role="3GE5qa" value="Item Definition" />
    <property role="TrG5h" value="Functions" />
    <node concept="19qcqe" id="6GvChUD1oSj" role="2lbk3h" />
    <node concept="2x4$T4" id="6GvChUD1oSk" role="2xH1$J" />
  </node>
  <node concept="2lbcm6" id="6GvChUD1oSl">
    <property role="TrG5h" value="Components" />
    <property role="3GE5qa" value="Item Definition" />
    <node concept="2lbezN" id="6GvChUD1oSm" role="2lbk3h">
      <property role="TrG5h" value="SYS" />
      <property role="DVXpC" value="System" />
      <node concept="3VMn$a" id="6GvChUD1oSn" role="2JHqPs">
        <node concept="3VMn$0" id="6GvChUD1oSo" role="3VMn$6">
          <node concept="3VMn$7" id="6GvChUD1oSp" role="3VMn$3">
            <property role="3VMn$Y" value="System" />
          </node>
          <node concept="3VMn$7" id="6GvChUD1oSq" role="3VMn$3">
            <property role="3VMn$Y" value="component" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2x4$Tb" id="6GvChUD1oSr" role="2xH1$J" />
  </node>
  <node concept="2lbcm6" id="6GvChUD1oSs">
    <property role="3GE5qa" value="Item Definition" />
    <property role="TrG5h" value="Data" />
    <node concept="19qcqe" id="6GvChUD1oSt" role="2lbk3h" />
    <node concept="2x4$T9" id="6GvChUD1oSu" role="2xH1$J" />
  </node>
  <node concept="2lbcm6" id="6GvChUD1oSv">
    <property role="TrG5h" value="Channels" />
    <property role="3GE5qa" value="Item Definition" />
    <node concept="3mlHNJ" id="6GvChUD1pSa" role="2lbk3h">
      <property role="TrG5h" value="Ch.24" />
      <node concept="3VMn$a" id="6GvChUD1pSb" role="2JHqPs" />
      <node concept="3$0O7b" id="6GvChUD1pSe" role="38xWUi">
        <ref role="122Z_O" to="wwjg:4ZIixnyTZWa" resolve="Cmp.15" />
      </node>
      <node concept="3$0O7b" id="6GvChUD1pSg" role="38xWUi">
        <ref role="122Z_O" to="wwjg:4ZIixnyTY6O" resolve="Cmp.10" />
      </node>
    </node>
    <node concept="2x4$Td" id="6GvChUD1oSx" role="2xH1$J" />
  </node>
  <node concept="2ndE_3" id="6GvChUD1oSy">
    <property role="2zzwJW" value="2" />
    <property role="3GE5qa" value="Item Definition" />
    <property role="TrG5h" value="System Diagram" />
    <property role="2x$e3D" value="7S54Cbo9KHg/Channels" />
    <node concept="3$0O7b" id="6GvChUD1oSz" role="3Vepgw">
      <ref role="122Z_O" to="wwjg:4ZIixnyTOmw" resolve="SYS" />
    </node>
    <node concept="3$0O6U" id="6GvChUD1oS$" role="1BT5$_">
      <ref role="3$0O6B" node="6GvChUD1oSv" resolve="Channels" />
    </node>
    <node concept="3$0O6U" id="6GvChUD1oS_" role="1BS0SA">
      <ref role="3$0O6B" node="6GvChUD1oSs" resolve="Data" />
    </node>
    <node concept="zGsxE" id="6GvChUD1oSA" role="zGsxT">
      <property role="1ueiNO" value="root.7055464025021850431" />
      <node concept="zGsxD" id="6GvChUD1oSB" role="zGsxH">
        <property role="2MHvPS" value="root.7055464025021850431" />
        <property role="zGsxJ" value="0.0;0.0;0.0;0.0" />
      </node>
    </node>
    <node concept="zGsxE" id="6GvChUD1pSj" role="zGsxT">
      <property role="1ueiNO" value="root.7719065439393123862" />
      <node concept="zGsxD" id="6GvChUD1pSk" role="zGsxH">
        <property role="2MHvPS" value="root.7719065439393123862" />
        <property role="zGsxJ" value="0.0;0.0;0.0;0.0" />
      </node>
    </node>
    <node concept="zGsxE" id="6GvChUD1pSq" role="zGsxT">
      <property role="1ueiNO" value="root.7719065439393123862_CT_Channels" />
      <node concept="zGsxD" id="6GvChUD1pSr" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090199_CT_Channels" />
        <property role="zGsxJ" value="142.0;71.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSs" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091903_CT_Channels" />
        <property role="zGsxJ" value="88.0;224.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSt" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089920_CT_Channels" />
        <property role="zGsxJ" value="264.0;20.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSu" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090098_CT_Channels" />
        <property role="zGsxJ" value="81.0;71.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSv" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090005_CT_Channels" />
        <property role="zGsxJ" value="20.0;71.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSw" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090550_CT_Channels" />
        <property role="zGsxJ" value="88.0;122.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSx" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091713_CT_Channels" />
        <property role="zGsxJ" value="20.0;224.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSy" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091357_CT_Channels" />
        <property role="zGsxJ" value="156.0;173.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSz" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089590_CT_Channels" />
        <property role="zGsxJ" value="20.0;20.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pS$" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090308_CT_Channels" />
        <property role="zGsxJ" value="203.0;71.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pS_" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090425_CT_Channels" />
        <property role="zGsxJ" value="20.0;122.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSA" role="zGsxH">
        <property role="2MHvPS" value="5759622431321974156_CT_Channels" />
        <property role="zGsxJ" value="224.0;224.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSB" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092174_CT_Channels" />
        <property role="zGsxJ" value="156.0;224.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSC" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091531_CT_Channels" />
        <property role="zGsxJ" value="224.0;173.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSD" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089774_CT_Channels" />
        <property role="zGsxJ" value="142.0;20.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSE" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090683_CT_Channels" />
        <property role="zGsxJ" value="156.0;122.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSF" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091191_CT_Channels" />
        <property role="zGsxJ" value="88.0;173.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSG" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089635_CT_Channels" />
        <property role="zGsxJ" value="81.0;20.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSH" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090824_CT_Channels" />
        <property role="zGsxJ" value="224.0;122.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSI" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091033_CT_Channels" />
        <property role="zGsxJ" value="20.0;173.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSJ" role="zGsxH">
        <property role="2MHvPS" value="7719065439393127946_CT_Channels" />
        <property role="zGsxJ" value="264.0;71.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSK" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975733_CT_Channels" />
        <property role="zGsxJ" value="88.0;275.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSL" role="zGsxH">
        <property role="2MHvPS" value="root.7719065439393123862_CT_Channels" />
        <property role="zGsxJ" value="0.0;0.0;0.0;0.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSM" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089843_CT_Channels" />
        <property role="zGsxJ" value="203.0;20.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pSN" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975169_CT_Channels" />
        <property role="zGsxJ" value="20.0;275.0;48.0;31.0" />
      </node>
    </node>
    <node concept="zGsxE" id="6GvChUD1pTF" role="zGsxT">
      <property role="1ueiNO" value="root.5759622431315019168_CT_Channels" />
      <node concept="zGsxD" id="6GvChUD1pTG" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089843_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;43.8;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTH" role="zGsxH">
        <property role="2MHvPS" value="7719065439393127946_CT_Channels" />
        <property role="zGsxJ" value="669.0;274.5;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTI" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091033_CT_Channels" />
        <property role="zGsxJ" value="305.0;519.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTJ" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090098_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="169.0;77.33333333333334;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTK" role="zGsxH">
        <property role="2MHvPS" value="5759622431315029963_CT_Channels" />
        <property role="zGsxJ" value="1062.0;546.5;172.0;268.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTL" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092174_CT_Channels-&gt;5759622431315066590_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTM" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089920_CT_Channels" />
        <property role="zGsxJ" value="308.5;104.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTN" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092634_CT_Channels_5759622431315092910_CT_Channels" />
        <property role="zGsxJ" value="20.0;49.0;94.0;20.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTO" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975169_CT_Channels" />
        <property role="zGsxJ" value="305.0;665.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTP" role="zGsxH">
        <property role="2MHvPS" value="5759622431315030031_CT_Channels" />
        <property role="zGsxJ" value="779.0;212.5;131.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTQ" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091531_CT_Channels" />
        <property role="zGsxJ" value="669.0;592.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTR" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089635_CT_Channels-&gt;5759622431315059191_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="65.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTS" role="zGsxH">
        <property role="2MHvPS" value="5759622431315029973_CT_Channels" />
        <property role="zGsxJ" value="20.0;122.0;109.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTT" role="zGsxH">
        <property role="2MHvPS" value="5759622431315066549_CT_Channels" />
        <property role="zGsxJ" value="2167.0;374.4;97.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTU" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089774_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="238.0;81.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTV" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090005_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;99.6;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTW" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092634_CT_Channels" />
        <property role="zGsxJ" value="20.0;158.0;134.0;89.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTX" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090308_CT_Channels-&gt;5759622431315067052_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTY" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089843_CT_Channels-&gt;5759622431315030056_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="152.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pTZ" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092461_CT_Channels" />
        <property role="zGsxJ" value="20.0;49.0;44.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU0" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090308_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="169.0;55.00000000000001;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU1" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089920_CT_Channels-&gt;5759622431315030056_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU2" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091033_CT_Channels-&gt;5759622431315066866_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU3" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090308_CT_Channels" />
        <property role="zGsxJ" value="308.5;177.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU4" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090824_CT_Channels" />
        <property role="zGsxJ" value="1286.0;336.5;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU5" role="zGsxH">
        <property role="2MHvPS" value="5759622431315059156_CT_Channels" />
        <property role="zGsxJ" value="54.0;30.166666666666664;169.0;122.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU6" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090683_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="238.0;174.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU7" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975169_CT_Channels-&gt;5759622431321974992_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="88.0;10.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU8" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975733_CT_Channels-&gt;5759622431321974992_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="88.0;32.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU9" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975733_CT_Channels" />
        <property role="zGsxJ" value="305.0;756.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUa" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089635_CT_Channels" />
        <property role="zGsxJ" value="965.5;665.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUb" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090550_CT_Channels-&gt;5759622431315066549_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUc" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090683_CT_Channels-&gt;5759622431315030084_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUd" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092381_CT_Channels" />
        <property role="zGsxJ" value="84.0;49.0;134.0;89.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUe" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090199_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="169.0;99.66666666666669;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUf" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090425_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="169.0;10.333333333333334;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUg" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090550_CT_Channels-&gt;5759622431315030084_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="100.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUh" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089774_CT_Channels" />
        <property role="zGsxJ" value="1689.5;204.9;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUi" role="zGsxH">
        <property role="2MHvPS" value="5759622431315066681_CT_Channels" />
        <property role="zGsxJ" value="481.5;435.0;59.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUj" role="zGsxH">
        <property role="2MHvPS" value="5759622431315067120_CT_Channels" />
        <property role="zGsxJ" value="1786.0;86.4;241.0;268.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUk" role="zGsxH">
        <property role="2MHvPS" value="5759622431315030084_CT_Channels" />
        <property role="zGsxJ" value="1850.5;374.4;100.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUl" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089590_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;211.2;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUm" role="zGsxH">
        <property role="2MHvPS" value="5759622431315066866_CT_Channels" />
        <property role="zGsxJ" value="405.0;508.0;112.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUn" role="zGsxH">
        <property role="2MHvPS" value="5759622431315029994_CT_Channels" />
        <property role="zGsxJ" value="20.0;195.0;132.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUo" role="zGsxH">
        <property role="2MHvPS" value="5759622431321974992_CT_Channels" />
        <property role="zGsxJ" value="135.0;664.5;88.0;54.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUp" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089843_CT_Channels" />
        <property role="zGsxJ" value="824.0;104.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUq" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090005_CT_Channels" />
        <property role="zGsxJ" value="965.5;223.5;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUr" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089590_CT_Channels" />
        <property role="zGsxJ" value="1289.5;665.0;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUs" role="zGsxH">
        <property role="2MHvPS" value="5759622431315066987_CT_Channels" />
        <property role="zGsxJ" value="405.0;20.0;102.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUt" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091357_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;54.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUu" role="zGsxH">
        <property role="2MHvPS" value="5759622431315085135_CT_Channels" />
        <property role="zGsxJ" value="20.0;195.0;178.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUv" role="zGsxH">
        <property role="2MHvPS" value="5759622431315066731_CT_Channels" />
        <property role="zGsxJ" value="465.0;581.0;152.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUw" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975549_CT_Channels_5759622431321976623_CT_Channels" />
        <property role="zGsxJ" value="20.0;49.0;94.0;20.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUx" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091903_CT_Channels" />
        <property role="zGsxJ" value="669.0;325.5;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUy" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091357_CT_Channels-&gt;5759622431315066681_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="59.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUz" role="zGsxH">
        <property role="2MHvPS" value="5759622431321950556_CT_Channels" />
        <property role="zGsxJ" value="449.5;654.0;123.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU$" role="zGsxH">
        <property role="2MHvPS" value="5759622431315066810_CT_Channels" />
        <property role="zGsxJ" value="405.0;362.0;102.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pU_" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091191_CT_Channels-&gt;5759622431315066925_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="203.0;32.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUA" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091191_CT_Channels-&gt;5759622431315066810_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUB" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092174_CT_Channels" />
        <property role="zGsxJ" value="962.0;336.5;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUC" role="zGsxH">
        <property role="2MHvPS" value="5759622431315029982_CT_Channels" />
        <property role="zGsxJ" value="20.0;49.0;103.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUD" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975549_CT_Channels" />
        <property role="zGsxJ" value="405.0;727.0;224.0;89.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUE" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091713_CT_Channels" />
        <property role="zGsxJ" value="305.0;446.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUF" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091713_CT_Channels-&gt;5759622431315066925_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="203.0;54.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUG" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091033_CT_Channels-&gt;5759622431315066925_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="203.0;76.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUH" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091903_CT_Channels-&gt;5759622431315066925_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="203.0;10.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUI" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975733_CT_Channels-&gt;5759622431321975549_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;38.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUJ" role="zGsxH">
        <property role="2MHvPS" value="5759622431315085102_CT_Channels" />
        <property role="zGsxJ" value="20.0;49.0;85.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUK" role="zGsxH">
        <property role="2MHvPS" value="5759622431315059124_CT_Channels" />
        <property role="zGsxJ" value="467.0;263.5;88.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUL" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091357_CT_Channels" />
        <property role="zGsxJ" value="669.0;446.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUM" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091191_CT_Channels" />
        <property role="zGsxJ" value="305.0;373.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUN" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091531_CT_Channels-&gt;5759622431315066731_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="152.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUO" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092381_CT_Channels_5759622431315093447_CT_Channels" />
        <property role="zGsxJ" value="20.0;49.0;94.0;20.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUP" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090199_CT_Channels-&gt;5759622431315059124_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUQ" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091713_CT_Channels-&gt;5759622431315066681_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUR" role="zGsxH">
        <property role="2MHvPS" value="5759622431315029956_CT_Channels" />
        <property role="zGsxJ" value="1396.0;133.4;238.0;267.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUS" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092174_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="37.0;43.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUT" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975169_CT_Channels-&gt;5759622431321950556_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUU" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089774_CT_Channels-&gt;5759622431315067120_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;128.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUV" role="zGsxH">
        <property role="2MHvPS" value="5759622431315066634_CT_Channels" />
        <property role="zGsxJ" value="797.8;303.0;37.0;98.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUW" role="zGsxH">
        <property role="2MHvPS" value="5759622431315059191_CT_Channels" />
        <property role="zGsxJ" value="812.0;654.0;65.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUX" role="zGsxH">
        <property role="2MHvPS" value="5759622431315085072_CT_Channels" />
        <property role="zGsxJ" value="125.0;49.0;96.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUY" role="zGsxH">
        <property role="2MHvPS" value="7719065439393127946_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;10.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pUZ" role="zGsxH">
        <property role="2MHvPS" value="root.5759622431315019168_CT_Channels" />
        <property role="zGsxJ" value="0.0;0.0;0.0;0.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV0" role="zGsxH">
        <property role="2MHvPS" value="5759622431321974156_CT_Channels" />
        <property role="zGsxJ" value="669.0;665.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV1" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090683_CT_Channels" />
        <property role="zGsxJ" value="1686.0;385.4;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV2" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090098_CT_Channels-&gt;5759622431315030031_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV3" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089590_CT_Channels-&gt;5759622431315029963_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="172.0;128.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV4" role="zGsxH">
        <property role="2MHvPS" value="5759622431315066590_CT_Channels" />
        <property role="zGsxJ" value="1131.0;325.5;34.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV5" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090005_CT_Channels-&gt;5759622431315030031_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="131.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV6" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091903_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;32.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV7" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090550_CT_Channels" />
        <property role="zGsxJ" value="2067.0;385.4;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV8" role="zGsxH">
        <property role="2MHvPS" value="713648978143336104_CT_Channels" />
        <property role="zGsxJ" value="20.0;49.0;100.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pV9" role="zGsxH">
        <property role="2MHvPS" value="5759622431315067052_CT_Channels" />
        <property role="zGsxJ" value="405.0;166.0;112.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVa" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090824_CT_Channels-&gt;5759622431315066590_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="34.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVb" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089635_CT_Channels-&gt;5759622431315029963_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;128.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVc" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090098_CT_Channels" />
        <property role="zGsxJ" value="672.5;223.5;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVd" role="zGsxH">
        <property role="2MHvPS" value="7719065439393127946_CT_Channels-&gt;5759622431315059124_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="88.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVe" role="zGsxH">
        <property role="2MHvPS" value="5759622431315030056_CT_Channels" />
        <property role="zGsxJ" value="435.0;93.0;152.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVf" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090425_CT_Channels" />
        <property role="zGsxJ" value="305.0;31.0;48.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVg" role="zGsxH">
        <property role="2MHvPS" value="5759622431321974156_CT_Channels-&gt;5759622431315059191_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVh" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090425_CT_Channels-&gt;5759622431315066987_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVi" role="zGsxH">
        <property role="2MHvPS" value="5759622431321974156_CT_Channels-&gt;5759622431321950556_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="123.0;20.5;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVj" role="zGsxH">
        <property role="2MHvPS" value="5759622431315066925_CT_Channels" />
        <property role="zGsxJ" value="20.0;325.0;203.0;98.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVk" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090824_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;155.39999999999998;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVl" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089920_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="169.0;32.66666666666667;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVm" role="zGsxH">
        <property role="2MHvPS" value="5759622431315085171_CT_Channels" />
        <property role="zGsxJ" value="20.0;122.0;102.0;53.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVn" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091531_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels_epPort" />
        <property role="zGsxJ" value="-12.0;76.0;12.0;12.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pVo" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090199_CT_Channels" />
        <property role="zGsxJ" value="308.5;274.5;41.0;31.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1pZi" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090098_CT_Channels-&gt;5759622431315030031_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1q11" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091531_CT_Channels-&gt;5759622431315066731_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1q2L" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091033_CT_Channels-&gt;5759622431315066925_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="255.0;407.0;255.0;534.5" />
      </node>
      <node concept="zGsxD" id="6GvChUD1q4y" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975169_CT_Channels-&gt;5759622431321950556_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1q6k" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090683_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="1666.0;313.4;1666.0;400.9" />
      </node>
      <node concept="zGsxD" id="6GvChUD1q87" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089843_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="1354.0;119.5;1354.0;183.2" />
      </node>
      <node concept="zGsxD" id="6GvChUD1q9V" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090824_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="1354.0;352.0;1354.0;294.79999999999995" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qbK" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090308_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="275.0;91.16666666666667;275.0;192.5" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qdA" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091191_CT_Channels-&gt;5759622431315066925_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="275.0;363.0;275.0;388.5" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qft" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090308_CT_Channels-&gt;5759622431315067052_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qhl" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091713_CT_Channels-&gt;5759622431315066925_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="265.0;385.0;265.0;461.5" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qje" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089635_CT_Channels-&gt;5759622431315059191_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1ql8" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090683_CT_Channels-&gt;5759622431315030084_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qn3" role="zGsxH">
        <property role="2MHvPS" value="5759622431321974156_CT_Channels-&gt;5759622431321950556_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qoZ" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090098_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="265.0;113.5;265.0;239.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qqW" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091357_CT_Channels-&gt;5759622431315066681_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qsU" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091713_CT_Channels-&gt;5759622431315066681_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1quT" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090199_CT_Channels-&gt;5759622431315059124_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qwT" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089774_CT_Channels-&gt;5759622431315067120_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qyU" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090005_CT_Channels-&gt;5759622431315030031_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1q$W" role="zGsxH">
        <property role="2MHvPS" value="7719065439393127946_CT_Channels-&gt;5759622431315059124_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qAZ" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090550_CT_Channels-&gt;5759622431315030084_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qD3" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091191_CT_Channels-&gt;5759622431315066810_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qF8" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089635_CT_Channels-&gt;5759622431315029963_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qHe" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092174_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qJl" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091531_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="747.0;607.5;747.0;385.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qLt" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090425_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qNA" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089774_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qPK" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090005_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qRV" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091033_CT_Channels-&gt;5759622431315066866_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qU7" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090824_CT_Channels-&gt;5759622431315066590_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qWk" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975169_CT_Channels-&gt;5759622431321974992_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1qYy" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089590_CT_Channels-&gt;5759622431315029956_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="1364.0;680.5;1364.0;350.6" />
      </node>
      <node concept="zGsxD" id="6GvChUD1r0L" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089920_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="285.0;68.83333333333334;285.0;119.5" />
      </node>
      <node concept="zGsxD" id="6GvChUD1r31" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091357_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="737.0;461.5;737.0;363.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1r5i" role="zGsxH">
        <property role="2MHvPS" value="5759622431321974156_CT_Channels-&gt;5759622431315059191_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1r7$" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089590_CT_Channels-&gt;5759622431315029963_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1r9R" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090199_CT_Channels-&gt;5759622431315059156_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="255.0;135.83333333333334;255.0;290.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rcb" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091903_CT_Channels-&gt;5759622431315066925_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rew" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975733_CT_Channels-&gt;5759622431321975549_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rgQ" role="zGsxH">
        <property role="2MHvPS" value="5759622431315092174_CT_Channels-&gt;5759622431315066590_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rjd" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089920_CT_Channels-&gt;5759622431315030056_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rl_" role="zGsxH">
        <property role="2MHvPS" value="5759622431321975733_CT_Channels-&gt;5759622431321974992_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="255.0;702.5;255.0;771.5" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rnY" role="zGsxH">
        <property role="2MHvPS" value="7719065439393127946_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="737.0;290.0;737.0;319.0" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rqo" role="zGsxH">
        <property role="2MHvPS" value="5759622431315091903_CT_Channels-&gt;5759622431315066634_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rsN" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090425_CT_Channels-&gt;5759622431315066987_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rvf" role="zGsxH">
        <property role="2MHvPS" value="5759622431315089843_CT_Channels-&gt;5759622431315030056_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
      <node concept="zGsxD" id="6GvChUD1rxG" role="zGsxH">
        <property role="2MHvPS" value="5759622431315090550_CT_Channels-&gt;5759622431315066549_CT_Channels_CT_Channels" />
        <property role="zGsxJ" value="" />
      </node>
    </node>
  </node>
  <node concept="2zckJ6" id="6GvChUD1oSC">
    <property role="3GE5qa" value="Item Definition" />
    <property role="TrG5h" value="Function Assignment" />
  </node>
  <node concept="2vPz$R" id="6GvChUD1oSD">
    <property role="TrG5h" value="Assumptions" />
    <property role="3GE5qa" value="Security Analysis" />
    <node concept="19qcqd" id="6GvChUD1oSE" role="2vPz$N" />
    <node concept="2xx57M" id="6GvChUD1oSF" role="2xH1$J" />
  </node>
  <node concept="2vPz$R" id="6GvChUD1oSG">
    <property role="TrG5h" value="Threat Scenarios" />
    <property role="3GE5qa" value="Security Analysis" />
    <node concept="19qcqd" id="6GvChUD1oSH" role="2vPz$N" />
    <node concept="3u6799" id="6GvChUD1oSI" role="2xH1$J" />
  </node>
  <node concept="2vPz$R" id="6GvChUD1oSJ">
    <property role="3GE5qa" value="Security Analysis" />
    <property role="TrG5h" value="Damage Scenarios" />
    <node concept="19qcqd" id="6GvChUD1oSK" role="2vPz$N" />
    <node concept="U8VUI" id="6GvChUD1oSL" role="2xH1$J" />
  </node>
  <node concept="2vPz$R" id="6GvChUD1oSM">
    <property role="TrG5h" value="Attack Steps" />
    <property role="3GE5qa" value="Security Analysis" />
    <node concept="19qcqd" id="6GvChUD1oSN" role="2vPz$N" />
    <node concept="2xx57I" id="6GvChUD1oSO" role="2xH1$J" />
  </node>
  <node concept="2vPz$R" id="6GvChUD1oSP">
    <property role="TrG5h" value="Controls" />
    <property role="3GE5qa" value="Security Analysis" />
    <node concept="19qcqd" id="6GvChUD1oSQ" role="2vPz$N" />
    <node concept="2xx57K" id="6GvChUD1oSR" role="2xH1$J" />
  </node>
  <node concept="2vPz$R" id="6GvChUD1oSS">
    <property role="3GE5qa" value="Security Analysis" />
    <property role="TrG5h" value="Control Scenarios" />
    <node concept="19qcqd" id="6GvChUD1oST" role="2vPz$N" />
    <node concept="2xx57Q" id="6GvChUD1oSU" role="2xH1$J" />
  </node>
  <node concept="1YSUgs" id="6GvChUD1oSV">
    <property role="3GE5qa" value="Item Definition" />
    <property role="TrG5h" value="Sequences" />
    <node concept="3VMn$a" id="6GvChUD1oSW" role="2JHqPs" />
  </node>
  <node concept="ypf9M" id="6GvChUD1oSX">
    <property role="TrG5h" value="Result Report" />
    <property role="3GE5qa" value="Reports" />
    <node concept="3x3r7t" id="6GvChUD1oSY" role="yp9Ks" />
    <node concept="ym6bn" id="6GvChUD1oSZ" role="yp9Ks">
      <ref role="39i2te" node="6GvChUD1oVJ" resolve="Project Info: MySecurityAnalysis [MySecurityAnalysis]" />
    </node>
    <node concept="yhPIs" id="6GvChUD1oT0" role="yp9Ks">
      <property role="2DBfkM" value="false" />
    </node>
    <node concept="ygSqK" id="6GvChUD1oT1" role="yp9Ks">
      <ref role="2HTkYB" node="6GvChUD1oSy" resolve="System Diagram" />
    </node>
    <node concept="yg4y$" id="6GvChUD1oT2" role="yp9Ks">
      <property role="ygo9M" value="All system elements are listed at the end of this document." />
    </node>
    <node concept="ymko6" id="6GvChUD1oT3" role="yp9Ks" />
    <node concept="2JOk35" id="6GvChUD1oT4" role="yp9Ks">
      <property role="2JOk3V" value=" ISO/SAE 21434 tables" />
    </node>
    <node concept="ckFx4" id="6GvChUD1oT5" role="yp9Ks" />
    <node concept="3UIwP1" id="6GvChUD1oT6" role="yp9Ks" />
    <node concept="3yVM0i" id="6GvChUD1oT7" role="yp9Ks" />
    <node concept="28bWPA" id="6GvChUD1oT8" role="yp9Ks">
      <property role="1CBqX7" value="2147483647" />
    </node>
    <node concept="ymko6" id="6GvChUD1oT9" role="yp9Ks" />
    <node concept="2JOk35" id="6GvChUD1oTa" role="yp9Ks">
      <property role="2JOk3V" value=" listing of security elements" />
    </node>
    <node concept="ygVOy" id="6GvChUD1oTb" role="yp9Ks" />
    <node concept="ygVO6" id="6GvChUD1oTc" role="yp9Ks" />
    <node concept="ygVO4" id="6GvChUD1oTd" role="yp9Ks" />
    <node concept="ygVO2" id="6GvChUD1oTe" role="yp9Ks" />
    <node concept="21ek43" id="6GvChUD1oTf" role="yp9Ks" />
    <node concept="ymko6" id="6GvChUD1oTg" role="yp9Ks" />
    <node concept="2JOk35" id="6GvChUD1oTh" role="yp9Ks">
      <property role="2JOk3V" value=" listing of system elements" />
    </node>
    <node concept="3xSvwN" id="6GvChUD1oTi" role="yp9Ks" />
    <node concept="3xdgjh" id="6GvChUD1oTj" role="yp9Ks" />
    <node concept="3xuwDp" id="6GvChUD1oTk" role="yp9Ks" />
    <node concept="3xttx0" id="6GvChUD1oTl" role="yp9Ks" />
    <node concept="3xttxm" id="6GvChUD1oTm" role="yp9Ks" />
    <node concept="3xttxO" id="6GvChUD1oTn" role="yp9Ks" />
    <node concept="3xttxa" id="6GvChUD1oTo" role="yp9Ks" />
  </node>
  <node concept="2Q15JU" id="6GvChUD1oTp">
    <property role="3GE5qa" value="Assistants" />
    <node concept="khATU" id="6GvChUD1oTq" role="kmFqQ" />
    <node concept="3aivMl" id="6GvChUD1oTr" role="2Q$E0J">
      <node concept="3aHhih" id="6GvChUD1oWT" role="3N3N22">
        <property role="3aHm6j" value="Components" />
        <property role="133MFP" value="Component" />
        <node concept="2Q16Lc" id="6GvChUD1oWU" role="3aHmvd">
          <ref role="2ClQv0" node="6GvChUD1oSm" resolve="SYS" />
          <node concept="raIdw" id="6GvChUD1oWV" role="2QGid4">
            <ref role="2ClRH1" to="qiur:4CQftq3lQja" resolve="C" />
            <ref role="2Dj$GC" node="6GvChUD1oSm" resolve="SYS" />
          </node>
          <node concept="raIdw" id="6GvChUD1oWW" role="2QGid4">
            <ref role="2ClRH1" to="qiur:4CQftq3lQjc" resolve="I" />
            <ref role="2Dj$GC" node="6GvChUD1oSm" resolve="SYS" />
          </node>
          <node concept="raIdw" id="6GvChUD1oWX" role="2QGid4">
            <ref role="2ClRH1" to="qiur:4CQftq3lQjb" resolve="Av" />
            <ref role="2Dj$GC" node="6GvChUD1oSm" resolve="SYS" />
          </node>
          <node concept="raIdw" id="6GvChUD1oWY" role="2QGid4">
            <ref role="2ClRH1" to="qiur:4sJG_Ma1JSM" resolve="Au" />
            <ref role="2Dj$GC" node="6GvChUD1oSm" resolve="SYS" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2Q15JU" id="6GvChUD1oTs">
    <property role="3GE5qa" value="Assistants" />
    <node concept="khC4Q" id="6GvChUD1oTt" role="kmFqQ" />
    <node concept="k5Jq$" id="6GvChUD1oTu" role="2Q$E0J">
      <node concept="3aHhih" id="6GvChUD1oX5" role="3N3N22">
        <property role="3aHm6j" value="Components" />
        <property role="133MFP" value="Component" />
        <node concept="2Q16Lc" id="6GvChUD1oX6" role="3aHmvd">
          <ref role="2ClQv0" node="6GvChUD1oSm" resolve="SYS" />
          <node concept="k5JqA" id="6GvChUD1oX7" role="2QGid4">
            <ref role="2ClRH1" to="qxlf:4CQftq3lQmG" resolve="TC.2" />
            <ref role="2Dj$GC" node="6GvChUD1oSm" resolve="SYS" />
          </node>
          <node concept="k5JqA" id="6GvChUD1oXa" role="2QGid4">
            <ref role="2ClRH1" to="qxlf:4CQftq3lQpp" resolve="TC.4" />
            <ref role="2Dj$GC" node="6GvChUD1oSm" resolve="SYS" />
          </node>
          <node concept="k5JqA" id="6GvChUD1oX9" role="2QGid4">
            <ref role="2ClRH1" to="qxlf:4CQftq3lQqK" resolve="TC.5" />
            <ref role="2Dj$GC" node="6GvChUD1oSm" resolve="SYS" />
          </node>
          <node concept="k5JqA" id="6GvChUD1oX8" role="2QGid4">
            <ref role="2ClRH1" to="qxlf:4CQftq3lQsu" resolve="TC.6" />
            <ref role="2Dj$GC" node="6GvChUD1oSm" resolve="SYS" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2Q15JU" id="6GvChUD1oTv">
    <property role="3GE5qa" value="Assistants" />
    <node concept="khAwF" id="6GvChUD1oTw" role="kmFqQ" />
    <node concept="2FpSCn" id="6GvChUD1oTx" role="2Q$E0J" />
  </node>
  <node concept="2vPz$R" id="6GvChUD1oTy">
    <property role="TrG5h" value="Risks" />
    <property role="3GE5qa" value="Security Analysis" />
    <node concept="19qcqd" id="6GvChUD1oTz" role="2vPz$N" />
    <node concept="2xx57O" id="6GvChUD1oT$" role="2xH1$J" />
  </node>
  <node concept="2H3I8p" id="6GvChUD1oT_">
    <property role="TrG5h" value="UN R155 Threats" />
    <property role="3F1M74" value="true" />
    <property role="3GE5qa" value="Checklists" />
    <node concept="2H0S5N" id="6GvChUD1oTA" role="2H0S4$">
      <property role="TrG5h" value="Threats regarding back-end servers related to vehicles in the field" />
      <node concept="3GSqTS" id="6GvChUD1oTB" role="2H0S5D">
        <property role="TrG5h" value="1 Back-end servers used as a means to attack a vehicle or extract data" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTC" role="2H0S5D">
        <property role="TrG5h" value="Abuse of privileges by staff (insider attack)" />
        <node concept="3VMn$a" id="6GvChUD1oTD" role="3GS99T" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTE" role="2H0S5D">
        <property role="TrG5h" value="Unauthorised internet access to the server (enabled for example by backdoors, unpatched system software vulnerabilities, SQL attacks or other means)" />
        <node concept="3VMn$a" id="6GvChUD1oTF" role="3GS99T" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTG" role="2H0S5D">
        <property role="TrG5h" value="Unauthorised physical access to the server (conducted by for example USB sticks or other media connecting to the server)" />
        <node concept="3VMn$a" id="6GvChUD1oTH" role="3GS99T" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oTI" role="2H0S5D">
        <property role="TrG5h" value="2 Services from back-end server being disrupted, affecting the operation of a vehicle " />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTJ" role="2H0S5D">
        <property role="TrG5h" value="Attack on back-end server stops it functioning, for example it prevents it from interacting with vehicles and providing services they rely on" />
        <node concept="3VMn$a" id="6GvChUD1oTK" role="3GS99T" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oTL" role="2H0S5D">
        <property role="TrG5h" value="3 Vehicle related data on back-end servers being lost or compromised" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTM" role="2H0S5D">
        <property role="TrG5h" value="Abuse of privileges by staff (insider attack)" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTN" role="2H0S5D">
        <property role="TrG5h" value="Loss of information in the cloud. Sensitive data may be lost due to attacks or accidents when data is stored by third-party cloud service providers" />
        <node concept="3VMn$a" id="6GvChUD1oTO" role="3GS99T" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTP" role="2H0S5D">
        <property role="TrG5h" value="Unauthorised internet access to the server (enabled for example by backdoors, unpatched system software vulnerabilities, SQL attacks or other means)" />
        <node concept="3VMn$a" id="6GvChUD1oTQ" role="3GS99T" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTR" role="2H0S5D">
        <property role="TrG5h" value="Unauthorised physical access to the server (conducted for example by USB sticks or other media connecting to the server)" />
        <node concept="3VMn$a" id="6GvChUD1oTS" role="3GS99T" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTT" role="2H0S5D">
        <property role="TrG5h" value="Information breach by unintended sharing of data (e.g. admin errors, storing data in servers in garages)" />
        <node concept="3VMn$a" id="6GvChUD1oTU" role="3GS99T" />
      </node>
    </node>
    <node concept="2H0S5N" id="6GvChUD1oTV" role="2H0S4$">
      <property role="TrG5h" value="Threats to vehicle regarding their communication channel" />
      <node concept="3GSqTS" id="6GvChUD1oTW" role="2H0S5D">
        <property role="TrG5h" value="4 Spoofing of messages or data received by the vehicle" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTX" role="2H0S5D">
        <property role="TrG5h" value="Spoofing of messages by impersonation (e.g. 802.11p V2X during platooning, GNSS messages, etc.)" />
        <node concept="3VMn$a" id="6GvChUD1oTY" role="3GS99T" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oTZ" role="2H0S5D">
        <property role="TrG5h" value="Sybil attack (in order to spoof other vehicles as if there are many vehicles on the road)" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oU0" role="2H0S5D">
        <property role="TrG5h" value="5 Communication channels used to conduct unauthorized manipulation, deletion or other amendments to vehicle held code/data" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oU1" role="2H0S5D">
        <property role="TrG5h" value="Communications channels permit code injection, for example tampered software binary might be injected into the communication stream" />
        <node concept="3VMn$a" id="6GvChUD1oU2" role="3GS99T" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oU3" role="2H0S5D">
        <property role="TrG5h" value="Communications channels permit manipulate of vehicle held data/code" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oU4" role="2H0S5D">
        <property role="TrG5h" value="Communication channels permit overwrite of vehicle held data/code" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oU5" role="2H0S5D">
        <property role="TrG5h" value="Communication channels permit erasure of vehicle held data/code" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oU6" role="2H0S5D">
        <property role="TrG5h" value="Communication channels permit introduction of data/code to the vehicle" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oU7" role="2H0S5D">
        <property role="TrG5h" value="6 Communication channels permit untrusted/unreliable messages to be accepted or are vulnerable to session hijacking/replay attacks" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oU8" role="2H0S5D">
        <property role="TrG5h" value="Accepting information from unreliable or untrusted source" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oU9" role="2H0S5D">
        <property role="TrG5h" value="Man in the middle attack/ session hijacking" />
        <node concept="3VMn$a" id="6GvChUD1oUa" role="3GS99T" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUb" role="2H0S5D">
        <property role="TrG5h" value="Replay attack, for example an attack against a communication gateway allows the attacker to downgrade software of an ECU or firmware of the gateway" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUc" role="2H0S5D">
        <property role="TrG5h" value="7 Information can be readily disclosed. For example through eavesdropping on communications or through allowing unauthorized access to sensitive files or folders" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUd" role="2H0S5D">
        <property role="TrG5h" value="Interception of information / interfering radiations / monitoring communications" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUe" role="2H0S5D">
        <property role="TrG5h" value="Gaining unauthorised access to files or data" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUf" role="2H0S5D">
        <property role="TrG5h" value="8 Denial of service attacks via communication channels to disrupt vehicle functions" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUg" role="2H0S5D">
        <property role="TrG5h" value="Sending a large number of garbage data to vehicle information system, so that it is unable to provide services in the normal manner" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUh" role="2H0S5D">
        <property role="TrG5h" value="Black hole attack, in order to disrupt communication between vehicles the attacker is able to block messages between the vehicles" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUi" role="2H0S5D">
        <property role="TrG5h" value="9 An unprivileged user is able to gain privileged access to vehicle systems" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUj" role="2H0S5D">
        <property role="TrG5h" value="An unprivileged user is able to gain privileged access, for example root access" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUk" role="2H0S5D">
        <property role="TrG5h" value="10 Viruses embedded in communication media are able to infect vehicle systems" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUl" role="2H0S5D">
        <property role="TrG5h" value="Virus embedded in communication media infects vehicle systems" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUm" role="2H0S5D">
        <property role="TrG5h" value="11 Messages received by the vehicle (for example X2V or diagnostic messages), or transmitted within it, contain malicious content" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUn" role="2H0S5D">
        <property role="TrG5h" value="Malicious internal (e.g. CAN) messages" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUo" role="2H0S5D">
        <property role="TrG5h" value="Malicious V2X messages, e.g. infrastructure to vehicle or vehicle-vehicle messages (e.g. CAM, DENM)" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUp" role="2H0S5D">
        <property role="TrG5h" value="Malicious diagnostic messages" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUq" role="2H0S5D">
        <property role="TrG5h" value="Malicious proprietary messages (e.g. those normally sent from OEM or component/system/function supplier)" />
      </node>
    </node>
    <node concept="2H0S5N" id="6GvChUD1oUr" role="2H0S4$">
      <property role="TrG5h" value="Threats to vehicles regarding their update procedures" />
      <node concept="3GSqTS" id="6GvChUD1oUs" role="2H0S5D">
        <property role="TrG5h" value="12 Misuse or compromise of update procedures" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUt" role="2H0S5D">
        <property role="TrG5h" value="Compromise of over the air software update procedures, This includes fabricating system update program or firmware" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUu" role="2H0S5D">
        <property role="TrG5h" value="Compromise of local/physical software update procedures. This includes fabricating system update program or firmware" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUv" role="2H0S5D">
        <property role="TrG5h" value="The software is manipulated before the update process (and is therefore corrupted), although the update process is intact" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUw" role="2H0S5D">
        <property role="TrG5h" value="Compromise of cryptographic keys of the software provider to allow invalid update" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUx" role="2H0S5D">
        <property role="TrG5h" value="13 It is possible to deny legitimate updates" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUy" role="2H0S5D">
        <property role="TrG5h" value="Denial of Service attack against update server or network to prevent rollout of critical software updates and/or unlock of customer specific features" />
      </node>
    </node>
    <node concept="2H0S5N" id="6GvChUD1oUz" role="2H0S4$">
      <property role="TrG5h" value="Threats to vehicles regarding unintended human actions" />
      <node concept="3GSqTS" id="6GvChUD1oU$" role="2H0S5D">
        <property role="TrG5h" value="14 Misconfiguration of equipment or systems by legitimate actor, e.g. owner or maintenance community" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oU_" role="2H0S5D">
        <property role="TrG5h" value="Misconfiguration of equipment by maintenance community or owner during installation/repair/use causing unintended consequence" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUA" role="2H0S5D">
        <property role="TrG5h" value="Erroneous use or administration of devices and systems (incl. OTA updates)" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUB" role="2H0S5D">
        <property role="TrG5h" value="15 Legitimate actors are able to take actions that would unwittingly facilitate a cyber-attack" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUC" role="2H0S5D">
        <property role="TrG5h" value="Innocent victim (e.g. owner, operator or maintenance engineer) being tricked into taking an action to unintentionally load malware or enable an attack" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUD" role="2H0S5D">
        <property role="TrG5h" value="Defined security procedures are not followed" />
      </node>
    </node>
    <node concept="2H0S5N" id="6GvChUD1oUE" role="2H0S4$">
      <property role="TrG5h" value="Threats to vehicles regarding their external connectivity and connections" />
      <node concept="3GSqTS" id="6GvChUD1oUF" role="2H0S5D">
        <property role="TrG5h" value="16 Manipulation of the connectivity of vehicle functions enables a cyber-attack, this can include telematics; systems that permit remote operations; and systems using short range wireless communications" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUG" role="2H0S5D">
        <property role="TrG5h" value="Manipulation of functions designed to remotely operate systems, such as remote key, immobiliser, and charging pile" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUH" role="2H0S5D">
        <property role="TrG5h" value="Manipulation of vehicle telematics (e.g. manipulate temperature measurement of sensitive goods, remotely unlock cargo doors)" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUI" role="2H0S5D">
        <property role="TrG5h" value="Interference with short range wireless systems or sensors" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUJ" role="2H0S5D">
        <property role="TrG5h" value="17 Hosted 3rd party software, e.g. entertainment applications, used as a means to attack vehicle systems" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUK" role="2H0S5D">
        <property role="TrG5h" value="Corrupted applications, or those with poor software security, used as a method to attack vehicle systems" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUL" role="2H0S5D">
        <property role="TrG5h" value="18" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUM" role="2H0S5D">
        <property role="TrG5h" value="External interfaces such as USB or other ports used as a point of attack, for example through code injection" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUN" role="2H0S5D">
        <property role="TrG5h" value="Media infected with a virus connected to a vehicle system" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUO" role="2H0S5D">
        <property role="TrG5h" value="Diagnostic access (e.g. dongles in OBD port) used to facilitate an attack, e.g. manipulate vehicle parameters (directly or indirectly)" />
      </node>
    </node>
    <node concept="2H0S5N" id="6GvChUD1oUP" role="2H0S4$">
      <property role="TrG5h" value="Potential targets of, or motivations for, an attack" />
      <node concept="3GSqTS" id="6GvChUD1oUQ" role="2H0S5D">
        <property role="TrG5h" value="19 Extraction of vehicle data/code" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUR" role="2H0S5D">
        <property role="TrG5h" value="Extraction of copyright or proprietary software from vehicle systems (product piracy)" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUS" role="2H0S5D">
        <property role="TrG5h" value="Unauthorized access to the owner’s privacy information such as personal identity, payment account information, address book information, location information, vehicle’s electronic ID, etc." />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUT" role="2H0S5D">
        <property role="TrG5h" value="Extraction of cryptographic keys" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oUU" role="2H0S5D">
        <property role="TrG5h" value="20 Manipulation of vehicle data/code" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUV" role="2H0S5D">
        <property role="TrG5h" value="Illegal/unauthorised changes to vehicle’s electronic ID" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUW" role="2H0S5D">
        <property role="TrG5h" value="Identity fraud. For example if a user wants to display another identity when communicating with toll systems, manufacturer backend" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUX" role="2H0S5D">
        <property role="TrG5h" value="Action to circumvent monitoring systems (e.g. hacking/ tampering/ blocking of messages such as ODR Tracker data, or number of runs)" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUY" role="2H0S5D">
        <property role="TrG5h" value="Data manipulation to falsify vehicle’s driving data (e.g. mileage, driving speed, driving directions, etc.)" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oUZ" role="2H0S5D">
        <property role="TrG5h" value="Unauthorised changes to system diagnostic data" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oV0" role="2H0S5D">
        <property role="TrG5h" value="21 Erasure of data/code" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oV1" role="2H0S5D">
        <property role="TrG5h" value="Unauthorized deletion/manipulation of system event logs" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oV2" role="2H0S5D">
        <property role="TrG5h" value="22 Introduction of malware" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oV3" role="2H0S5D">
        <property role="TrG5h" value="Introduce malicious software or malicious software activity" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oV4" role="2H0S5D">
        <property role="TrG5h" value="23 Introduction of new software or overwrite existing software" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oV5" role="2H0S5D">
        <property role="TrG5h" value="Fabrication of software of the vehicle control system or information system" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oV6" role="2H0S5D">
        <property role="TrG5h" value="24 Disruption of systems or operations" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oV7" role="2H0S5D">
        <property role="TrG5h" value="Denial of service, for example this may be triggered on the internal network by flooding a CAN bus, or by provoking faults on an ECU via a high rate of messaging" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oV8" role="2H0S5D">
        <property role="TrG5h" value="25 Manipulation of vehicle parameters" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oV9" role="2H0S5D">
        <property role="TrG5h" value="Unauthorized access of falsify the configuration parameters of vehicle’s key functions, such as brake data, airbag deployed threshold, etc." />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVa" role="2H0S5D">
        <property role="TrG5h" value="Unauthorized access of falsify the charging parameters, such as charging voltage, charging power, battery temperature, etc." />
      </node>
    </node>
    <node concept="2H0S5N" id="6GvChUD1oVb" role="2H0S4$">
      <property role="TrG5h" value="Potential vulnerabilities that could be exploited if not sufficiently protected or hardened" />
      <node concept="3GSqTS" id="6GvChUD1oVc" role="2H0S5D">
        <property role="TrG5h" value="26 Cryptographic technologies can be compromised or are insufficiently applied" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVd" role="2H0S5D">
        <property role="TrG5h" value="Combination of short encryption keys and long period of validity enables attacker to break encryption" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVe" role="2H0S5D">
        <property role="TrG5h" value="Insufficient use of cryptographic algorithms to protect sensitive systems" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVf" role="2H0S5D">
        <property role="TrG5h" value="Using already or soon to be deprecated cryptographic algorithms" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oVg" role="2H0S5D">
        <property role="TrG5h" value="27 Parts or supplies could be compromised to permit vehicles to be attacked" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVh" role="2H0S5D">
        <property role="TrG5h" value="Hardware or software, engineered to enable an attack or fails to meet design criteria to stop an attack" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oVi" role="2H0S5D">
        <property role="TrG5h" value="28 Software or hardware development permits vulnerabilities" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVj" role="2H0S5D">
        <property role="TrG5h" value="Software bugs. The presence of software bugs can be a basis for potential exploitable vulnerabilities. This is particularly true if software has not been tested to verify that known bad code/bugs is not present and reduce the risk of unknown bad code/bugs being present." />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVk" role="2H0S5D">
        <property role="TrG5h" value="Using remainders from development (e.g. debug ports, JTAG ports, microprocessors, development certificates, developer passwords, …) can permit access to ECUs or permit attackers to gain higher privileges" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oVl" role="2H0S5D">
        <property role="TrG5h" value="29 Network design introduces vulnerabilities" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVm" role="2H0S5D">
        <property role="TrG5h" value="Superfluous internet ports left open, providing access to network systems" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVn" role="2H0S5D">
        <property role="TrG5h" value="Circumvent network separation to gain control. Specific example is the use of unprotected gateways, or access points (such as truck-trailer gateways), to circumvent protections and gain access to other network segments to perform malicious acts, such as sending arbitrary CAN bus messages" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oVo" role="2H0S5D">
        <property role="TrG5h" value="30 Physical loss of data can occur" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVp" role="2H0S5D">
        <property role="TrG5h" value="Damage caused by a third party. Sensitive data may be lost or compromised due to physical damages in cases of traffic accident or theft" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVq" role="2H0S5D">
        <property role="TrG5h" value="Loss from DRM (digital right management) conflicts. User data may be deleted due to DRM issues" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVr" role="2H0S5D">
        <property role="TrG5h" value="The (integrity of) sensitive data may be lost due to IT components wear and tear, causing potential cascading issues (in case of key alteration, for example)" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oVs" role="2H0S5D">
        <property role="TrG5h" value="31 Unintended transfer of data can occur" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVt" role="2H0S5D">
        <property role="TrG5h" value="Information breach. Private or sensitive data may be leaked when the car changes user (e.g. is sold or is used as hire vehicle with new hirers)" />
      </node>
      <node concept="3GSqTS" id="6GvChUD1oVu" role="2H0S5D">
        <property role="TrG5h" value="32 Physical manipulation of systems can enable an attack" />
      </node>
      <node concept="2H0S4X" id="6GvChUD1oVv" role="2H0S5D">
        <property role="TrG5h" value="Manipulation of OEM hardware, e.g. unauthorised hardware added to a vehicle to enable &quot;man-in-the-middle&quot; attack" />
      </node>
    </node>
    <node concept="3VMn$a" id="6GvChUD1oVw" role="2JHqPs">
      <node concept="3VMn$0" id="6GvChUD1oVx" role="3VMn$6">
        <node concept="3VMn$7" id="6GvChUD1oVy" role="3VMn$3">
          <property role="3VMn$Y" value="List" />
        </node>
        <node concept="3VMn$7" id="6GvChUD1oVz" role="3VMn$3">
          <property role="3VMn$Y" value="of" />
        </node>
        <node concept="3VMn$7" id="6GvChUD1oV$" role="3VMn$3">
          <property role="3VMn$Y" value="examples" />
        </node>
        <node concept="3VMn$7" id="6GvChUD1oV_" role="3VMn$3">
          <property role="3VMn$Y" value="of" />
        </node>
        <node concept="3VMn$7" id="6GvChUD1oVA" role="3VMn$3">
          <property role="3VMn$Y" value="vulnerability" />
        </node>
        <node concept="3VMn$7" id="6GvChUD1oVB" role="3VMn$3">
          <property role="3VMn$Y" value="or" />
        </node>
        <node concept="3VMn$7" id="6GvChUD1oVC" role="3VMn$3">
          <property role="3VMn$Y" value="attack" />
        </node>
        <node concept="3VMn$7" id="6GvChUD1oVD" role="3VMn$3">
          <property role="3VMn$Y" value="method" />
        </node>
        <node concept="3VMn$7" id="6GvChUD1oVE" role="3VMn$3">
          <property role="3VMn$Y" value="to" />
        </node>
        <node concept="3VMn$7" id="6GvChUD1oVF" role="3VMn$3">
          <property role="3VMn$Y" value="threats." />
        </node>
      </node>
    </node>
  </node>
  <node concept="2Q15JU" id="6GvChUD1oVG">
    <property role="3GE5qa" value="Assistants" />
    <node concept="CEhHY" id="6GvChUD1oVH" role="kmFqQ" />
    <node concept="CENT2" id="6GvChUD1oVI" role="2Q$E0J" />
  </node>
  <node concept="3eC5pO" id="6GvChUD1oVJ">
    <property role="TrG5h" value="Project Info" />
    <node concept="$sJSu" id="6GvChUD1oVK" role="$s4ey">
      <property role="TrG5h" value="Project Data" />
      <node concept="$sJSp" id="6GvChUD1oVL" role="$tzTT">
        <property role="TrG5h" value="Target Of Evaluation" />
        <node concept="38D_9Z" id="6GvChUD1oVM" role="X3RNv">
          <node concept="3VMn$a" id="6GvChUD1oVN" role="38D_my">
            <node concept="3VMn$0" id="6GvChUD1oVO" role="3VMn$6">
              <node concept="3VMn$7" id="6GvChUD1oVP" role="3VMn$3">
                <property role="3VMn$Y" value="" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="$sJSp" id="6GvChUD1oVQ" role="$tzTT">
        <property role="TrG5h" value="Project" />
        <node concept="38D_9Z" id="6GvChUD1oVR" role="X3RNv">
          <node concept="3VMn$a" id="6GvChUD1oVS" role="38D_my">
            <node concept="3VMn$0" id="6GvChUD1oVT" role="3VMn$6">
              <node concept="3VMn$7" id="6GvChUD1oVU" role="3VMn$3">
                <property role="3VMn$Y" value="" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="$sJSp" id="6GvChUD1oVV" role="$tzTT">
        <property role="TrG5h" value="Contact (Department)" />
        <node concept="38D_9Z" id="6GvChUD1oVW" role="X3RNv">
          <node concept="3VMn$a" id="6GvChUD1oVX" role="38D_my">
            <node concept="3VMn$0" id="6GvChUD1oVY" role="3VMn$6">
              <node concept="3VMn$7" id="6GvChUD1oVZ" role="3VMn$3">
                <property role="3VMn$Y" value="" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="$sJSp" id="6GvChUD1oW0" role="$tzTT">
        <property role="TrG5h" value="Contact (Security Expert)" />
        <node concept="38D_9Z" id="6GvChUD1oW1" role="X3RNv">
          <node concept="3VMn$a" id="6GvChUD1oW2" role="38D_my">
            <node concept="3VMn$0" id="6GvChUD1oW3" role="3VMn$6">
              <node concept="3VMn$7" id="6GvChUD1oW4" role="3VMn$3">
                <property role="3VMn$Y" value="" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="$sJSp" id="6GvChUD1oW5" role="$tzTT">
        <property role="TrG5h" value="Editor" />
        <node concept="38D_9Z" id="6GvChUD1oW6" role="X3RNv">
          <node concept="3VMn$a" id="6GvChUD1oW7" role="38D_my">
            <node concept="3VMn$0" id="6GvChUD1oW8" role="3VMn$6">
              <node concept="3VMn$7" id="6GvChUD1oW9" role="3VMn$3">
                <property role="3VMn$Y" value="" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="$sJSp" id="6GvChUD1oWa" role="$tzTT">
        <property role="TrG5h" value="Deadline" />
        <node concept="38D_9Z" id="6GvChUD1oWb" role="X3RNv">
          <node concept="3VMn$a" id="6GvChUD1oWc" role="38D_my">
            <node concept="3VMn$0" id="6GvChUD1oWd" role="3VMn$6">
              <node concept="3VMn$7" id="6GvChUD1oWe" role="3VMn$3">
                <property role="3VMn$Y" value="" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="$sJSh" id="6GvChUD1oWf" role="$s4ey" />
    <node concept="$sJSu" id="6GvChUD1oWg" role="$s4ey">
      <property role="TrG5h" value="Status" />
      <node concept="$sJSp" id="6GvChUD1oWh" role="$tzTT">
        <property role="TrG5h" value="Risk Analyis Status" />
        <node concept="38D_9Z" id="6GvChUD1oWi" role="X3RNv">
          <node concept="3VMn$a" id="6GvChUD1oWj" role="38D_my">
            <node concept="3VMn$0" id="6GvChUD1oWk" role="3VMn$6">
              <node concept="3VMn$7" id="6GvChUD1oWl" role="3VMn$3">
                <property role="3VMn$Y" value="" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="$sJSh" id="6GvChUD1oWm" role="$s4ey" />
    <node concept="39leHu" id="6GvChUD1oWn" role="$s4ey">
      <property role="TrG5h" value="Version History" />
      <node concept="2mR0d" id="6GvChUD1oWo" role="2mR6f">
        <node concept="2mR0e" id="6GvChUD1oWp" role="2mR6i">
          <node concept="2opHn" id="6GvChUD1oWq" role="2hY46" />
        </node>
        <node concept="2mR0e" id="6GvChUD1oWr" role="2mR6i">
          <node concept="2opHn" id="6GvChUD1oWs" role="2hY46" />
        </node>
        <node concept="2mR0e" id="6GvChUD1oWt" role="2mR6i">
          <node concept="2opHn" id="6GvChUD1oWu" role="2hY46" />
        </node>
        <node concept="2mR0e" id="6GvChUD1oWv" role="2mR6i">
          <node concept="2opHn" id="6GvChUD1oWw" role="2hY46" />
        </node>
      </node>
      <node concept="2ozQQ" id="6GvChUD1oWx" role="2hO6J">
        <property role="TrG5h" value="Revision" />
      </node>
      <node concept="2ozQQ" id="6GvChUD1oWy" role="2hO6J">
        <property role="TrG5h" value="Date" />
      </node>
      <node concept="2ozQQ" id="6GvChUD1oWz" role="2hO6J">
        <property role="TrG5h" value="Authors" />
      </node>
      <node concept="2ozQQ" id="6GvChUD1oW$" role="2hO6J">
        <property role="TrG5h" value="Description" />
      </node>
    </node>
    <node concept="$sJSh" id="6GvChUD1oW_" role="$s4ey" />
    <node concept="39leHu" id="6GvChUD1oWA" role="$s4ey">
      <property role="TrG5h" value="Documents" />
      <node concept="2mR0d" id="6GvChUD1oWB" role="2mR6f">
        <node concept="2mR0e" id="6GvChUD1oWC" role="2mR6i">
          <node concept="2opHn" id="6GvChUD1oWD" role="2hY46" />
        </node>
        <node concept="2mR0e" id="6GvChUD1oWE" role="2mR6i">
          <node concept="2opHn" id="6GvChUD1oWF" role="2hY46" />
        </node>
        <node concept="2mR0e" id="6GvChUD1oWG" role="2mR6i">
          <node concept="2opHn" id="6GvChUD1oWH" role="2hY46" />
        </node>
        <node concept="2mR0e" id="6GvChUD1oWI" role="2mR6i">
          <node concept="2opHn" id="6GvChUD1oWJ" role="2hY46" />
        </node>
        <node concept="2mR0e" id="6GvChUD1oWK" role="2mR6i">
          <node concept="2opHn" id="6GvChUD1oWL" role="2hY46" />
        </node>
      </node>
      <node concept="2ozQQ" id="6GvChUD1oWM" role="2hO6J">
        <property role="TrG5h" value="Content" />
      </node>
      <node concept="2ozQQ" id="6GvChUD1oWN" role="2hO6J">
        <property role="TrG5h" value="Date" />
      </node>
      <node concept="2ozQQ" id="6GvChUD1oWO" role="2hO6J">
        <property role="TrG5h" value="Version" />
      </node>
      <node concept="2ozQQ" id="6GvChUD1oWP" role="2hO6J">
        <property role="TrG5h" value="Filename" />
      </node>
      <node concept="2ozQQ" id="6GvChUD1oWQ" role="2hO6J">
        <property role="TrG5h" value="Origin" />
      </node>
    </node>
    <node concept="$sJSh" id="6GvChUD1oWR" role="$s4ey" />
  </node>
</model>

